import React, { Component } from 'react';
import SettingsAddEmployee from './SettingsAddEmployee';
import SettingsListContainer from './SettingsListContainer';
import SettingsEmployee from './SettingsEmployee';

class Settings extends Component {
  renderList() {
    return (
      this.props.employees
        /* sorting ascending to employee first letter of surname */
        /* using .localeCompare to sort through czech characters */
        .sort((a, b) => {
          return a.surname.substr(0, 1).localeCompare(b.surname.substr(0, 1));
        })
        .map(item => <SettingsEmployee key={item.id} item={item} />)
    );
  }

  render() {
    return (
      <div className="container">
        <SettingsAddEmployee />
        <SettingsListContainer renderList={this.renderList()} />
      </div>
    );
  }
}

export default Settings;
