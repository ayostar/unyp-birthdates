import React, { Component } from 'react';
import Card from './common/Card';
import Employee from './common/Employee';
import moment from 'moment';
import { winter, spring, summer, autumn } from '../img/index';

class Landing extends Component {
  constructor() {
    super();
    this.getSeason = this.getSeason.bind(this);
  }

  getSeason(number) {
    if (number === 1 || number === 2 || number === 12) {
      return winter;
    } else if (number === 3 || number === 4 || number === 5) {
      return spring;
    } else if (number === 6 || number === 7 || number === 8) {
      return summer;
    } else {
      return autumn;
    }
  }

  getEmployeeWeek(month, day) {
    return moment(`${moment().format('YYYY')} ${month} ${day}`, 'YYYY MM DD')
      .startOf('isoweek')
      .format('ww');
  }

  getMonthEmployees(number) {
    return this.props.employees
      .filter(item => Number(item.month) === number)
      .sort((a, b) => {
        return a.day - b.day;
      })
      .map((item, index) => {
        return <Employee employee={item} key={index} />;
      });
  }

  getWeekEmployees(number) {
    return this.props.employees
      .filter(item => {
        return number === this.getEmployeeWeek(item.month, item.day);
      })
      .map((item, index) => {
        return <Employee employee={item} key={index} />;
      });
  }

  render() {
    const { weekNumber, month, monthVerbal } = this.props.date;
    const currentMonth = Number(month);
    const nextMonth = Number(month) + 1;
    const nextMonthVerbal = moment()
      .add(1, 'months')
      .format('MMMM');
    return (
      <div className="container">
        <div className="row">
          <Card
            col="col s12 m6"
            color="accent-color"
            list={this.getWeekEmployees(weekNumber)}
            title="This week"
          />
          <Card
            col="col s12 m6"
            color="teal lighten-1"
            bg={this.getSeason(currentMonth)}
            textColor={
              currentMonth === 6 || currentMonth === 7 || currentMonth === 8 ? 'black' : 'white'
            }
            list={this.getMonthEmployees(currentMonth)}
            title={`B-days in ${monthVerbal}`}
          />
        </div>
        <div className="row">
          <Card
            col="col s12 m6"
            color="purple lighten-4"
            bg={this.getSeason(nextMonth)}
            textColor={nextMonth === 6 || nextMonth === 7 || nextMonth === 8 ? 'black' : 'white'}
            list={this.getMonthEmployees(nextMonth)}
            offset="offset-m6"
            title={`B-days in ${nextMonthVerbal}`}
          />
        </div>
      </div>
    );
  }
}

export default Landing;
