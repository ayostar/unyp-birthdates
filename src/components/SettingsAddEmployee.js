import React, { Component } from 'react';
import fire from '../fire';

class AddEmployee extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      surname: '',
      day: '',
      month: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { name, surname, day, month } = this.state;

    if (name !== '' && surname !== '' && day !== '' && month !== '') {
      const dbRef = fire.database().ref('employees');
      const newEmployee = { name, surname, day, month };
      dbRef.push(newEmployee);

      this.setState({
        name: '',
        surname: '',
        day: '',
        month: ''
      });
    }
  }

  handleChange(e) {
    const element = e.target.name;
    this.setState({
      [element]: e.target.value
    });
  }

  render() {
    const { name, surname, day, month } = this.state;
    return (
      <div className="add-container blue-grey lighten-4 z-depth-1">
        <h5>Add new employee</h5>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            {/* NAME */}
            <div className="input-group col s4">
              <input
                placeholder="name"
                name="name"
                type="text"
                value={name}
                onChange={this.handleChange}
              />
            </div>
            {/* SURNAME */}
            <div className="input-group col s4">
              <input
                placeholder="surname"
                name="surname"
                type="text"
                value={surname}
                onChange={this.handleChange}
              />
            </div>
            {/* DAY */}
            <div className="input-group col s2">
              <input
                placeholder="day"
                name="day"
                type="number"
                min={1}
                max={31}
                value={day}
                onChange={this.handleChange}
              />
            </div>
            {/* MONTH */}
            <div className="input-group col s2">
              <input
                placeholder="month"
                name="month"
                type="number"
                min={1}
                max={12}
                value={month}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="row center">
            <button className="btn" type="submit">
              ADD
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default AddEmployee;
