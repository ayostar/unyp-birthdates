import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyCv8hmIeFeWURXcwN5EUTDk3Md3BlXZpSs',
  authDomain: 'unypbirthdates.firebaseapp.com',
  databaseURL: 'https://unypbirthdates.firebaseio.com',
  projectId: 'unypbirthdates',
  storageBucket: 'unypbirthdates.appspot.com',
  messagingSenderId: '76283904238'
};

const fire = firebase.initializeApp(config);

export default fire;
