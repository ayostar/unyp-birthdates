import React from 'react';

const ListContainer = ({ renderList }) => {
  return (
    <div className="list-container">
      <ul className="collection with-header light-blue lighten-4">
        <li className="collection-header light-blue lighten-4">
          <h5>Employee List</h5>
        </li>
        {renderList}
      </ul>
    </div>
  );
};

export default ListContainer;
