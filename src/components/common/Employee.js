import React from 'react';
import moment from 'moment';

const Employee = ({ employee, short }) => {
  const { day, month, name, surname } = employee;
  const currentYear = moment().format('YYYY');
  let dayVerbal = moment(`${currentYear} ${month} ${day}`, 'YYYY MM DD').format('dddd');
  let shortCol = '';
  if (short) {
    dayVerbal = dayVerbal.substr(0, 3);
    shortCol = 'col s2';
  } else {
    shortCol = 'col s3';
  }
  return (
    <div>
      <div className="row employee">
        <p className="col s2">{`${day}.${month}`}</p>
        <p className={shortCol}>{`${dayVerbal}`}</p>
        <p className="col s7">{`${surname} ${name}`}</p>
      </div>
      <div className="divider" />
    </div>
  );
};

export default Employee;
