import React from 'react';

const Card = ({ title, list, bg, color, textColor, col, offset }) => {
  return (
    <div className={`${col} ${offset === undefined ? '' : offset}`}>
      <div className={`card ${color}`} style={{ backgroundImage: `url(${String(bg)})` }}>
        <div className={`card-content ${textColor === 'black' ? 'black-text' : 'white-text'}`}>
          <span className="card-title center">{title}</span>
          <div className="list-container">{list}</div>
        </div>
      </div>
    </div>
  );
};

export default Card;
