import React from 'react';
import { NavLink, Link } from 'react-router-dom';

const Header = () => {
  return (
    <div className="container-fluid">
      <nav>
        <div className="nav-wrapper default-primary-color">
          <Link to="/" className="brand-logo center">
            <i className="material-icons">cake</i>
            UNYP Birthdates
          </Link>
          <ul id="nav-mobile" className="right">
            <li>
              <NavLink to="/calendar">
                <i className="material-icons">date_range</i>
              </NavLink>
            </li>
            <li>
              <NavLink to="/settings">
                <i className="material-icons">settings</i>
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Header;
