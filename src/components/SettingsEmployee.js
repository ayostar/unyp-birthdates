import React, { Component } from 'react';
import fire from '../fire';

class SettingsEmployee extends Component {
  removeEmployee(id) {
    const rootRef = fire.database().ref(`/employees/${id}`);
    rootRef.remove();
  }

  render() {
    const { id, day, month, name, surname } = this.props.item;
    return (
      <li key={id} className="collection-item">
        <div className="row employee-list-row">
          <div className="col s4">{`${surname} ${name}`}</div>
          <div className="col s2">{`${day}.${month}`}</div>
          <div className="right">
            <button className="btn" onClick={() => this.removeEmployee(id)}>
              <i className="material-icons">delete</i>
            </button>
          </div>
        </div>
      </li>
    );
  }
}

export default SettingsEmployee;
