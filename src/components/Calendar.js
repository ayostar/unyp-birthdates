import React, { Component } from 'react';
import Card from './common/Card';
import Employee from './common/Employee';
import { winter, spring, summer, autumn } from '../img/index';

class Calendar extends Component {
  getMonthEmployees = number => {
    return this.props.employees
      .filter(item => Number(item.month) === number)
      .sort((a, b) => {
        return a.day - b.day;
      })
      .map((item, index) => {
        return <Employee employee={item} key={index} short />;
      });
  };

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Card
            col={'col m4'}
            color={'blue lighten-2'}
            textColor="white"
            title="January"
            bg={winter}
            list={this.getMonthEmployees(1)}
          />
          <Card
            col={'col m4'}
            color={'blue lighten-2'}
            textColor="white"
            title="February"
            bg={winter}
            list={this.getMonthEmployees(2)}
          />
          <Card
            col={'col m4'}
            color={'green lighten-2'}
            textColor="white"
            title="March"
            bg={spring}
            list={this.getMonthEmployees(3)}
          />
        </div>
        <div className="row">
          <Card
            col={'col m4'}
            color={'green lighten-2'}
            textColor="white"
            title="April"
            bg={spring}
            list={this.getMonthEmployees(4)}
          />
          <Card
            col={'col m4'}
            color={'green lighten-2'}
            textColor="white"
            title="May"
            bg={spring}
            list={this.getMonthEmployees(5)}
          />
          <Card
            col={'col m4'}
            color={'red lighten-2'}
            textColor="black"
            title="June"
            bg={summer}
            list={this.getMonthEmployees(6)}
          />
        </div>
        <div className="row">
          <Card
            col={'col m4'}
            color={'red lighten-2'}
            textColor="black"
            title="July"
            bg={summer}
            list={this.getMonthEmployees(7)}
          />
          <Card
            col={'col m4'}
            color={'red lighten-2'}
            textColor="black"
            title="August"
            bg={summer}
            list={this.getMonthEmployees(8)}
          />
          <Card
            col={'col m4'}
            color={'orange lighten-2'}
            textColor="white"
            title="September"
            bg={autumn}
            list={this.getMonthEmployees(9)}
          />
        </div>
        <div className="row">
          <Card
            col={'col m4'}
            color={'orange lighten-2'}
            textColor="white"
            title="October"
            bg={autumn}
            list={this.getMonthEmployees(10)}
          />
          <Card
            col={'col m4'}
            color={'orange lighten-2'}
            textColor="white"
            title="November"
            bg={autumn}
            list={this.getMonthEmployees(11)}
          />
          <Card
            col={'col m4'}
            color={'blue lighten-2'}
            textColor="white"
            title="December"
            bg={winter}
            list={this.getMonthEmployees(12)}
          />
        </div>
      </div>
    );
  }
}

export default Calendar;
