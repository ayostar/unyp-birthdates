import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import moment from 'moment';
import fire from '../fire';

import Header from './Header';
import HeaderInfoBar from './HeaderInfoBar';
import Landing from './Landing';
import Settings from './Settings';
import Calendar from './Calendar';
import Login from './Login';

/* TODO: CONVERT TO BULMA */
/* TODO: set all color variables appropriately */
/* TODO: add ability to modify dates and/or names of current employees */
/* TODO: add ability to logout */

class App extends Component {
  constructor() {
    super();
    this.state = {
      isAuthenticated: false,
      employees: [],
      date: {
        day: moment().format('DD'),
        dayVerbal: moment().format('dddd'),
        weekNumber: moment()
          .startOf('isoweek')
          .format('ww'),
        month: moment().format('MM'),
        monthVerbal: moment().format('MMMM'),
        year: moment().format('YYYY')
      }
    };
  }

  componentWillMount() {
    const employeesRef = fire
      .database()
      .ref('/employees')
      .orderByChild('surname');
    employeesRef.on('value', snapshot => {
      let employees = snapshot.val();
      let newState = [];
      for (let employee in employees) {
        newState.push({
          id: employee,
          name: employees[employee].name,
          surname: employees[employee].surname,
          day: employees[employee].day,
          month: employees[employee].month
        });
      }
      this.setState({ employees: newState });
    });
  }

  handleLogin = bool => {
    this.setState((prevState, props) => {
      return { isAuthenticated: bool };
    });
  };

  renderRoutes = () => {
    const { employees, date } = this.state;
    return (
      <div>
        <Route path="*" component={Header} />
        <Route path="*" component={props => <HeaderInfoBar {...props} date={date} />} />
        <Route
          path="/"
          exact
          component={props => <Landing {...props} employees={employees} date={date} />}
        />
        <Route
          path="/settings"
          component={props => <Settings {...props} employees={employees} />}
        />
        <Route
          path="/calendar"
          component={props => <Calendar {...props} employees={employees} date={date} />}
        />
      </div>
    );
  };

  render() {
    const { isAuthenticated } = this.state;
    return (
      <Router>
        {isAuthenticated ? (
          this.renderRoutes()
        ) : (
          <Route to="*" component={props => <Login {...props} handleLogin={this.handleLogin} />} />
        )}
      </Router>
    );
  }
}

export default App;
