import React from 'react';

const HeaderInfoBar = ({ date }) => {
  const { day, dayVerbal, monthVerbal, year } = date;
  return (
    <div className="infobar-container z-depth-1">
      <div className="infobar-section">
        Today is {`${dayVerbal} - ${day}. ${monthVerbal} ${year}`}
      </div>
    </div>
  );
};

export default HeaderInfoBar;
