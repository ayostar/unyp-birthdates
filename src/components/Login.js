import React, { Component } from 'react';
import { password } from '../config';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      pass: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.state.pass === password ? this.props.handleLogin(true) : console.log('heslo NE');
    this.setState({ pass: '' });
  }

  handleChange(e) {
    this.setState({ pass: e.target.value });
  }

  render() {
    const { pass } = this.state;
    return (
      <div className="pass-container">
        <form className="pass-form" onSubmit={this.handleSubmit}>
          {/* pass */}
          <input
            placeholder="password"
            name="pass"
            type="password"
            value={pass}
            onChange={this.handleChange}
            className="pass-input"
          />
          <button className="btn" type="submit">
            LOGIN
          </button>
        </form>
      </div>
    );
  }
}

export default Login;
